/********************************************************************
	日期:	2013/12/03
	文件: 	JWIni.cpp JWIni.h
	作者:	Jim Wen
	版本:	V1.0
	
	功能:	对Ini文件的操作做一个简单的封装,满足一般参数文件配置的使用
	说明:	无
*********************************************************************/

#include "stdafx.h"
#include "JWIni.h"

/**
 *功能:默认构造函数
 *参数:无
 *返回:无
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
JWIni::JWIni()
{
}

/**
 *功能:带参数构造函数,传入参数必须是带full path的路径,
	   例如当前目录下1.ini文件传入时必须写成.\\1.ini,
	   否则Windows会默认搜索Windows文件夹,关于INI文件
	   的搜索机制请参见MSDN
 *参数:szFileName--带完整文件路径的ini文件名
 *返回:无
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
JWIni::JWIni( LPCTSTR szFileName )
{
	m_csFileName = szFileName;
}

/**
*功能:设置文件路径,传入参数必须是带full path的路径,
      例如当前目录下1.ini文件传入时必须写成.\\1.ini,
      否则Windows会默认搜索Windows文件夹,关于INI文件
	  的搜索机制请参见MSDN
 *参数:szFileName--带完整文件路径的ini文件名
 *返回:无
 *其他:2014/01/08 By 文洲 Ver1.0
**/
void JWIni::SetIniFile( LPCTSTR szFileName )
{
	m_csFileName = szFileName;
}

JWIni::~JWIni()
{

}

/**
 *功能:设置Section的值,注意Section原来的值会被删除,重新置为传入的值
 *参数:szSectionName--section名
	   szString--key=value形式的键值对,相邻之间用\0隔开,最后以\0\0收尾
 *返回:成功为TRUE,失败为FALSE
 *其他:2013/12/03 By Jim Wen Ver1.0

 *示例:
 *原来文件内容为:
 [section1]
 key1=1
 key2=2
 *调用函数
 SetSection("section1", TEXT("key3=3\0key4=4\0\0"))
 *后,文件内容为:
 [section1]
 key3=3
 key4=4
**/
BOOL JWIni::SetSection(LPCTSTR szSectionName, LPCTSTR szString)
{
	return WritePrivateProfileSection(szSectionName, szString, m_csFileName.GetBuffer(256));
}

/**
 *功能:设置Section下的Key的string值
 *参数:szSectionName--section名
	   szKeyName--Key名
	   szString--以\0结尾的String的值
 *返回:成功为TRUE,失败为FALSE
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
BOOL JWIni::SetString(LPCTSTR szSectionName, LPCTSTR szKeyName, LPCTSTR szString)
{
	return WritePrivateProfileString(szSectionName, szKeyName, szString, m_csFileName.GetBuffer(256));
}

/**
 *功能:设置Section下Key的值,做参数配置的时候用的并不多，
	   可用于保存自定义数据,如二进制序列，如果你愿意，
	   甚至可以使用他来保存一幅图像数据
 *参数:szSectionName--section名
	   szKeyName--key名
	   lpStruct--要保存的数据起始地址
	   uSizeStruct--数据的字节数
 *返回:成功返回TRUE,失败返回FALSE
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
BOOL JWIni::SetStruct(LPCTSTR szSectionName, LPCTSTR szKeyName, LPVOID lpStruct, const UINT uSizeStruct)
{
	return WritePrivateProfileStruct(szSectionName, szKeyName, lpStruct, uSizeStruct, m_csFileName.GetBuffer(256));
}

/**
 *功能:设置Section下的Key的int值
 *参数:szSectionName--section名
	   szKeyName--Key名
	   nValue--int值
 *返回:成功为TRUE,失败为FALSE
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
BOOL JWIni::SetInt(LPCTSTR szSectionName, LPCTSTR szKeyName, const INT nValue)
{
	CString csTemp;
	csTemp.Format(TEXT("%d"), nValue);

	return SetString(szSectionName, szKeyName, csTemp.GetBuffer(256));
}

/**
 *功能:设置Section下的Key的double值
 *参数:szSectionName--section名
	   szKeyName--Key名
	   dbValue--double值
 *返回:成功为TRUE,失败为FALSE
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
BOOL JWIni::SetDouble(LPCTSTR szSectionName, LPCTSTR szKeyName, const DOUBLE dbValue)
{
	CString csTemp;
	csTemp.Format(TEXT("%.6f"), dbValue);
	
	return SetString(szSectionName, szKeyName, csTemp.GetBuffer(256));
}

/**
 *功能:获得Section的值
 *参数:szSectionName--section名
	   array--接受Section中key的值数组,结果中只包含key对应的值不包括key
 *返回:无
 *其他:2013/12/03 By Jim Wen Ver1.0

 *示例:
 *原来文件内容为:
 [section1]
 key3=3
 key4=4
 *调用函数
 GetSection(TEXT("section1"), array)
 *后,接受数组项内容依次为:
 "3"
 "4"
**/
void JWIni::GetSection(LPCTSTR szSectionName, CStringArray &array)
{
	TCHAR szBuffer[256];

	//注意这个函数返回key=value形式的键值对,相邻之间用\0隔开,最后以\0\0收尾
	/*示例:
		原来文件内容为:
		[section1]
		key3=3
		key4=4
		*调用函数
		GetPrivateProfileSection(TEXT("section1"),...)
		后,返回内容为:
		"key3=3\0key4=4\0\0"
	*/
	GetPrivateProfileSection(szSectionName, szBuffer, 256, m_csFileName.GetBuffer(256));

	//将得到的字符串处理成以CStringArray数组,主要是操作指针
	CString csTemp;
	int lengthTemp, posTemp;
	TCHAR *szDest = szBuffer;
	
	while(1)
	{
		csTemp = szDest;
		lengthTemp = csTemp.GetLength();
		
		if (lengthTemp > 0)
		{
			posTemp = csTemp.Find('=');
			array.Add(csTemp.Right(lengthTemp-1-posTemp).GetBuffer(256));
			szDest += lengthTemp+1;//跳转到下一个\0后
		}
		else
		{
			break;
		}
	}
}

/**
 *功能:获得所有的Section
 *参数:array--接受所有Section名的数组
 *返回:无
 *其他:2013/12/03 By Jim Wen Ver1.0
 *示例:
 *原来文件内容为:
 [section1]
 key3=3
 [section2]
 key4=4
 *调用函数
 GetSectionNames
 *后,接受数组内容为:
 "section1" "section2"
**/
void JWIni::GetSectionNames(CStringArray &array)
{
	TCHAR szBuffer[256];
	GetPrivateProfileSectionNames(szBuffer, 256, m_csFileName.GetBuffer(256));
	
	//将得到的字符串处理成以CStringArray数组,主要是操作指针
	TCHAR szTemp[256];
	int lengthTemp;
	TCHAR *szDest = szBuffer;
	
	while(1)
	{
		lstrcpy(szTemp, szDest);
		lengthTemp = lstrlen(szTemp);
		
		if (lengthTemp > 0)
		{
			array.Add(szTemp);
			szDest += lengthTemp+1;//跳转到下一个\0后
		}
		else
		{
			break;
		}
	}
}

/**
 *功能:获得Section下Key的String值
 *参数:szSectionName--section名
	   szKeyName--key名
	   szDefault--默认default值
 *返回:对应的String值
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
CString JWIni::GetString(LPCTSTR szSectionName, LPCTSTR szKeyName, LPCTSTR szDefault/*=TEXT("")*/)
{
	TCHAR szBuffer[512];
	GetPrivateProfileString(szSectionName, szKeyName, szDefault, szBuffer, 512, m_csFileName.GetBuffer(256));
	
	return szBuffer;
}

/**
 *功能:获得Section下Key的保存的自定义数据值
 *参数:szSectionName--section名
	   szKeyName--key名
	   lpStruct--接受数据的内存的起始地址
	   uSizeStruct--数据的字节数
 *返回:成功返回TRUE,失败返回FALSE
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
BOOL JWIni::GetStruct(LPCTSTR szSectionName, LPCTSTR szKeyName, LPVOID lpStruct, const UINT uSizeStruct)
{
	return GetPrivateProfileStruct(szSectionName, szKeyName, lpStruct, uSizeStruct, m_csFileName.GetBuffer(256));
}

/**
 *功能:获得Section下Key的int值
 *参数:szSectionName--section名
	   szKeyName--key名
	   nDefault--默认default值
 *返回:对应的int值
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
INT JWIni::GetInt(LPCTSTR szSectionName, LPCTSTR szKeyName, const INT nDefault/*=0*/)
{
	TCHAR szBuffer[256];
	CString csTemp;
	csTemp.Format(TEXT("%d"), nDefault);
	GetPrivateProfileString(szSectionName, szKeyName, csTemp.GetBuffer(256), szBuffer, 256, m_csFileName.GetBuffer(256));
	
	return _ttoi(szBuffer);
}

/**
 *功能:获得Section下Key的double值
 *参数:szSectionName--section名
	   szKeyName--key名
	   dbDefault--默认default值
 *返回:对应的double值
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
DOUBLE JWIni::GetDouble(LPCTSTR szSectionName, LPCTSTR szKeyName, const DOUBLE dbDefault/*=0*/)
{
	TCHAR szBuffer[256];
	CString csTemp;
	csTemp.Format(TEXT("%f"), dbDefault);
	GetPrivateProfileString(szSectionName, szKeyName, csTemp.GetBuffer(256), szBuffer, 256, m_csFileName.GetBuffer(256));
	
	return _tcstod(szBuffer, NULL);
}