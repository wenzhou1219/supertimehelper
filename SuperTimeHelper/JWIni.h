#ifndef JWINI_H_H_H
#define JWINI_H_H_H

class JWIni  
{
public:
	JWIni();
	JWIni(LPCTSTR szFileName);
	virtual ~JWIni();
	void SetIniFile(LPCTSTR szFileName);
	BOOL SetSection(LPCTSTR szSectionName, LPCTSTR szString);
	BOOL SetString(LPCTSTR szSectionName, LPCTSTR szKeyName, LPCTSTR szString);
	BOOL SetStruct(LPCTSTR szSectionName, LPCTSTR szKeyName, LPVOID lpStruct, const UINT uSizeStruct);
	BOOL SetInt(LPCTSTR szSectionName, LPCTSTR szKeyName, const INT nValue);
	BOOL SetDouble(LPCTSTR szSectionName, LPCTSTR szKeyName, const DOUBLE dbValue);
	void GetSection(LPCTSTR szSectionName, CStringArray &array);
	void GetSectionNames(CStringArray &array);
	CString GetString(LPCTSTR szSectionName, LPCTSTR szKeyName, LPCTSTR szDefault=TEXT(""));
	BOOL GetStruct(LPCTSTR szSectionName, LPCTSTR szKeyName, LPVOID lpStruct, const UINT uSizeStruct);
	INT GetInt(LPCTSTR szSectionName, LPCTSTR szKeyName, const INT nDefault=0);
	DOUBLE GetDouble(LPCTSTR szSectionName, LPCTSTR szKeyName, const DOUBLE dbDefault=0);
	
private:
	CString m_csFileName;
};

#endif
