
// SuperTimeHelper.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "SuperTimeHelper.h"
#include "SuperTimeHelperDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define one	TEXT("0x8edcf627, 0xc466, 0x406b, 0x9e, 0xb, 0x2b, 0x94, 0xf6, 0xb5, 0x52, 0xa3")

// CSuperTimeHelperApp

BEGIN_MESSAGE_MAP(CSuperTimeHelperApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CSuperTimeHelperApp 构造

CSuperTimeHelperApp::CSuperTimeHelperApp()
{
	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}

CSuperTimeHelperApp::~CSuperTimeHelperApp()
{
	delete m_pMainWnd;
}


// 唯一的一个 CSuperTimeHelperApp 对象

CSuperTimeHelperApp theApp;


// CSuperTimeHelperApp 初始化

BOOL CSuperTimeHelperApp::InitInstance()
{
	//应用程序单实例化
	m_OneHandle = ::CreateMutex(NULL, FALSE, one);//handle为声明的HANDLE类型的全局变量  
	if(GetLastError()==ERROR_ALREADY_EXISTS)  
	{
		MessageBox(NULL, TEXT("应用程序已经在运行"), TEXT("警告"), MB_OK);  
		return FALSE;  
	}

	//获得当前可执行文件路径和配置文件路径
	TCHAR szBuffer[MAX_PATH] ;											//临时存储可执行文件全路径
	TCHAR szDrive[10] ;													//可执行文件盘符
	TCHAR szDir[256] ;													//可执行文件目录
	TCHAR szFilename[64] ;												//可执行文件文件名
	TCHAR szExt[10] ;													//可执行文件文件后缀

	GetModuleFileName(NULL, szBuffer, MAX_PATH) ;
	_tsplitpath(szBuffer, szDrive, szDir, szFilename, szExt) ;			//分割全路径

	m_csExeFileName = szBuffer;
	m_csConfigFileName.Format(TEXT("%s%sConfig.ini"), szDrive, szDir) ;	//得到配置文件路径

	//判断配置文件是否存在
	WIN32_FIND_DATA fd;
	if (INVALID_HANDLE_VALUE == FindFirstFile(m_csConfigFileName.GetBuffer(MAX_PATH), &fd))
	{
		MessageBox(NULL, TEXT("配置文件丢失,程序无法启动"), TEXT("错误"), MB_OK | MB_ICONERROR);
		return FALSE;
	}

	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	AfxEnableControlContainer();

	//采用非模态对话框便于程序一启动就隐藏对话框
	m_pMainWnd = new CSuperTimeHelperDlg;
	((CSuperTimeHelperDlg *)m_pMainWnd)->Create(IDD_SUPERTIMEHELPER_DIALOG, NULL);

	return TRUE;
}

int CSuperTimeHelperApp::ExitInstance()
{
	// TODO: 在此添加专用代码和/或调用基类

	// 收尾工作
	if (NULL != m_OneHandle)
	{
		CloseHandle(m_OneHandle);
	}

	return CWinAppEx::ExitInstance();
}
