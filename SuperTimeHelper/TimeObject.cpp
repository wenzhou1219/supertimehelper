// TimeObject.cpp : 实现文件
//

#include "stdafx.h"
#include "SuperTimeHelper.h"
#include "SuperTimeHelperDlg.h"
#include "TimeObject.h"

#include "DlgAlarm.h"
#include "shutdown.h"


// CTimeObject

CTimeObject::CTimeObject()
{
	m_time = CTime::GetCurrentTime();
	m_timeLazyMinute = 0;

	m_timeFreq = TIMEFREQ_DAY;
	m_timeType = TIMETYPE_ALARM;

	m_info = TEXT("闹钟");

	m_timeCount = 0;
}

CTimeObject::CTimeObject( const CTime time, const UINT timeLazyMinute, 
						  const ENUM_TIMEFREQ timeFreq, const ENUM_TIMETYPE timeType, 
						  const CString info, 
						  const UINT timeCount/*=0*/ )
{
	m_time = time;
	m_timeLazyMinute = timeLazyMinute;

	m_timeFreq = timeFreq;
	m_timeType = timeType;

	m_info = info;

	m_timeCount = timeCount;
}

CTimeObject::~CTimeObject()
{
}

/************************************************************************/
/* 功能:对于定时闹钟、关机、注销、重启进行判断是否定时到达              */
/************************************************************************/
BOOL CTimeObject::IsOnTime( CTime currentTime )
{
	if (m_timeType == TIMETYPE_ALARMONSTART || m_timeType == TIMETYPE_ALARMONSHUT)
	{
		return (currentTime.GetYear() == m_time.GetYear() && 
				currentTime.GetMonth() == m_time.GetMonth() && 
				currentTime.GetDay() == m_time.GetDay());
	}
	else
	{
		return (m_time+CTimeSpan(0, 0, m_timeLazyMinute*m_timeCount, 0)) == currentTime;
	}
}

void CTimeObject::UpdateNextTime()
{
	switch (m_timeFreq)
	{
	case TIMEFREQ_WEEK:
		m_time += CTimeSpan(7, 0, 0, 0);
		break;

	case TIMEFREQ_DAY:
	case TIMEFREQ_WEEKDAY:
		if (m_time.GetDayOfWeek() == 6 && m_timeFreq == TIMEFREQ_WEEKDAY)//工作日到周5自动跳到下个星期1
		{
			m_time += CTimeSpan(3, 0, 0, 0);
		}
		else
		{
			m_time += CTimeSpan(1, 0, 0, 0);
		}
		break;

	case TIMEFREQ_HOUR:
		m_time += CTimeSpan(0, 1, 0, 0);
		break;
	}
}

void CTimeObject::ResponseOnTime(CTime currentTime, UINT nIndex=0)
{
	CDlgAlarm dlgAlarm;

	if (IsOnTime(currentTime))//定时响应
	{
		switch (m_timeType)
		{
		case TIMETYPE_ALARM://闹钟
			dlgAlarm.SetInfo(m_info, m_timeLazyMinute);

			if (IDOK == dlgAlarm.DoModal())//非懒人模式响应
			{
				//清空已响应次数,设置下一次响应时间
				m_timeCount = 0;

				//设置间隔设置的频率时间再响应
				UpdateNextTime();

				//等待设置的频率间隔时间后再响应
				((CSuperTimeHelperDlg *)AfxGetMainWnd())->m_timeListBox.ModifyObject(nIndex, 
																					 m_time, m_timeLazyMinute,
																					 m_timeFreq, m_timeType,
																					 m_info, m_timeCount);
			}
			else//懒人模式响应
			{
				//已响应次数加1
				m_timeCount += 1;

				//等待设置的懒人延迟时间后再响应
				((CSuperTimeHelperDlg *)AfxGetMainWnd())->m_timeListBox.ModifyObject(nIndex, 
																					 m_time, m_timeLazyMinute,
																					 m_timeFreq, m_timeType,
																					 m_info, m_timeCount);
			}

			//保存配置
			((CSuperTimeHelperDlg *)AfxGetMainWnd())->m_timeListBox.ExportToFile(((CSuperTimeHelperDlg *)AfxGetMainWnd())->m_configIni);
			break;

		case TIMETYPE_SHUTDOWN:
		case TIMETYPE_LOGOFF:
		case TIMETYPE_RESTART:
			//设置间隔设置的频率时间再响应
			UpdateNextTime();

			//等待设置的频率间隔时间后再响应
			((CSuperTimeHelperDlg *)AfxGetMainWnd())->m_timeListBox.ModifyObject(nIndex, 
				m_time, m_timeLazyMinute,
				m_timeFreq, m_timeType,
				m_info, m_timeCount);

			//保存配置
			((CSuperTimeHelperDlg *)AfxGetMainWnd())->m_timeListBox.ExportToFile(((CSuperTimeHelperDlg *)AfxGetMainWnd())->m_configIni);

			if (m_timeType == TIMETYPE_SHUTDOWN)
			{
				//关机
				shut_down(SHUTDOWN);
			}
			else if (m_timeType == TIMETYPE_LOGOFF)
			{
				//注销
				shut_down(LOGOFF);
			}
			else
			{
				//重启
				shut_down(RESTART);
			}
			break;
		}
	}
}

void CTimeObject::ResponseOnStartOrShut( CTime currentTime, UINT nIndex )
{
	if (IsOnTime(currentTime))//定时响应
	{
		UpdateNextTime();

		//缓存未改变前的数据
		ENUM_TIMETYPE timeType = m_timeType;
		CString csInfo = m_info;

		//等待设置的频率间隔时间后再响应
		((CSuperTimeHelperDlg *)AfxGetMainWnd())->m_timeListBox.ModifyObject(nIndex, 
			m_time, m_timeLazyMinute,
			m_timeFreq, m_timeType,
			m_info, m_timeCount);

		//保存配置
		((CSuperTimeHelperDlg *)AfxGetMainWnd())->m_timeListBox.ExportToFile(((CSuperTimeHelperDlg *)AfxGetMainWnd())->m_configIni);

		//弹出提示
		if (timeType == TIMETYPE_ALARMONSHUT)
		{
			AfxGetMainWnd()->MessageBox(csInfo, TEXT("关机提醒"), MB_OK | MB_SYSTEMMODAL | MB_ICONWARNING);
		}
		else if(timeType == TIMETYPE_ALARMONSTART)
		{
			AfxGetMainWnd()->MessageBox(csInfo, TEXT("开机提醒"), MB_OK | MB_SYSTEMMODAL | MB_ICONWARNING);
		}
	}
}

CString CTimeObject::ToString()
{
	CString csResult;

	csResult.Format(TEXT("time:%I64d lazy:%u freq:%u type:%u count:%u info:%s"),
							m_time,
							m_timeLazyMinute,
							m_timeFreq,
							m_timeType,
							m_timeCount,
							m_info.GetBuffer(0) == NULL ? TEXT(" ") : m_info.GetBuffer(0));

	return csResult;
}
