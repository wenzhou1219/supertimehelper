#pragma once


// CDlgAlarm 对话框

class CDlgAlarm : public CDialog
{
	DECLARE_DYNAMIC(CDlgAlarm)

public:
	CDlgAlarm(CWnd* pParent = NULL);   // 标准构造函数
	void SetInfo(CString info, UINT timeLazyMinute);
	virtual ~CDlgAlarm();

// 对话框数据
	enum { IDD = IDD_ALARM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CString m_info;
	UINT m_timeLazyMinute;
	virtual BOOL OnInitDialog();
};
